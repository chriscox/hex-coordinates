/*
  hex_validate.cpp
      Test if cartesian to hexagon coordinate conversion can be simplified further,
          and make sure the results match previous conversions.

  ccox: defining a hexagon needs a minimum of 3 quantized coordinates, so 3 floor() ops may be minimal.


-Ofast
IEEE compliance doesn't seem to affect times, but loop unrolling helps

hex Chambers: 1.2433 sec
hex Cox: 1.1715 sec

*/

#include <stdlib.h>
#include <stdio.h>
#include <cmath>
#include <chrono>
#include <functional>

/******************************************************************************/
/******************************************************************************/

// Baseline code
// https://www.redblobgames.com/grids/hexagons/more-pixel-to-hex.html
// attributed to Charles Chambers
// The "simplification" given appears to be incorrect (q-r is wrong), and mostly just rearranges terms

static
void hex_chambers( const double x, const double y, int64_t &q, int64_t &r )
{
    const double sqrt3 = sqrt(3.0);
    double t = sqrt3 * y + 1;
    double temp1 = floor( t + x );
    double temp2 = floor( t - x );
    double temp3 = floor( 2 * x + 1 );
    q = int64_t( floor((temp3 + temp1) / 3) );
    r = int64_t( floor((temp1 + temp2) / 3) );
}

/******************************************************************************/

// modified by Chris Cox to use only 3 floor calls
//  still could differ by 1 when FP rounding kicks in (ex: N.9999999999)

static
void hex_cox( const double x, const double y, int64_t &q, int64_t &r )
{
    const double sqrt3 = sqrt(3.0);
    double t = sqrt3 * y + 1;           // scaled y, plus phase
    double temp1 = floor( t + x );      // (y+x) diagonal, this calc needs floor
    double temp2 = ( t - x );           // (y-x) diagonal, no floor needed
    double temp3 = ( 2 * x + 1 );       // scaled horizontal, no floor needed, needs +1 to get correct phase
    double qf = (temp1 + temp3) / 3.0;  // pseudo x with fraction
    double rf = (temp1 + temp2) / 3.0;  // pseudo y with fraction
    q = int64_t( floor( qf ) );         // pseudo x, quantized and thus requires floor
    r = int64_t( floor( rf ) );         // pseudo y, quantized and thus requires floor
}

/******************************************************************************/
/******************************************************************************/

// and now all the testing framework bits

static
int64_t hash64( int64_t x )
{
    return int64_t( std::hash<int64_t>{}(x) );
}

/******************************************************************************/

// returns 0.0..1.0
static
double hash_to_double(int64_t x)
{
    const int BITS = 36;
    const uint64_t MASK = (1ULL << BITS)-1;
    const double scale = 1.0 / double(MASK);
    
    double result = (uint64_t(x) & MASK) * scale;
    return result;
}

/******************************************************************************/

// returns -1.0..1.0
static
double hash64_float( int64_t x )
{
    int64_t temp = hash64(x);
    double result = (2.0 * hash_to_double(temp)) - 1.0;
    return result;
}

/******************************************************************************/
/******************************************************************************/

static
void compare_hex_calls( const double x, const double y, const char *label )
{
    int64_t q0, r0;
    hex_chambers( x, y, q0, r0 );
    
    int64_t q2, r2;
    hex_cox( x, y, q2, r2 );
    
    if (q0 != q2)
        {
        printf("hex_cox %s failed q (input %f, %f)\n", label, x, y );
        hex_cox( x, y, q2, r2 );      // to allow for debugging
        }
    if (r0 != r2)
        {
        printf("hex_cox %s failed r (input %f, %f)\n", label, x, y );
        hex_cox( x, y, q2, r2 );      // to allow for debugging
        }
}

/******************************************************************************/

// make sure my changes match the original function
static
void validate_hex_coords()
{
    const int64_t seed = 0x42424242;
    const int64_t y_offset = hash64( 0x1BADBEEFFEEDLL );
    const int64_t randomTestLimit = 100000000LL;
    const double fineScale = 10.0;
    const double largeScale = 1.0e7;
    
    const double gridRadius = 4.0;
    const int64_t gridDensity = 10000LL;
    const double gridScale = (2.0 * gridRadius) / gridDensity;
    

    // fine grained grid centered around zero
    for ( int64_t i = 0; i < gridDensity; ++i ) {
        const double x = i*gridScale - gridRadius;        // -gridRadius..+gridRadius
        
        for ( int64_t j = 0; j < gridDensity; ++j ) {
            const double y = j*gridScale - gridRadius;
            compare_hex_calls(x,y,"grid");
        }
    }
    
    
    // fine grained random, centered around zero
    for ( int64_t i = 0; i < randomTestLimit; ++i ) {
        const double x = fineScale * hash64_float(i+seed);        // +-scale
        const double y = fineScale * hash64_float((i+seed) ^ y_offset);
        compare_hex_calls(x,y,"fine");
    }
    
    
    // much larger random, centered around zero
    for ( int64_t i = 0; i < randomTestLimit; ++i ) {
        const double x = largeScale * hash64_float(i+seed);        // +-scale
        const double y = largeScale * hash64_float((i+seed) ^ y_offset);
        compare_hex_calls(x,y,"large");
    }

}

/******************************************************************************/
/******************************************************************************/

template< typename CLK >
struct clock_std_chrono
{
    static double seconds() {
                    auto now = CLK::now();
                    auto now_ns = std::chrono::time_point_cast<std::chrono::nanoseconds>(now).time_since_epoch().count();
                    double now_seconds = now_ns * 1.0e-9;
                    return now_seconds; }
};

typedef clock_std_chrono< std::chrono::high_resolution_clock > highres_clock;

/******************************************************************************/

struct PrintTimer
{
    PrintTimer (const char *message)
        {
        fMessage = message;
        fStartTime = highres_clock::seconds();
        }

    ~PrintTimer ()
        {
        double sec = highres_clock::seconds() - fStartTime;
        printf ("%s: %0.4f sec\n", fMessage, sec);
        fflush(stdout);
        }

private:
		const char *fMessage;
		double fStartTime;
};

/******************************************************************************/

// Seperate loops so the compiler won't share code between the calls and we can get accurate numbers.
// How they perform depends a lot on how the compiler vectorizes the code.
// But test2 has fewer instructions and fewer dependent calculations (without vectorization it has fewer FP function calls).

static
void profile_hex_conversions()
{
    const double gridRadius = 4.0;
    const int64_t gridDensity = 20000LL;
    const double gridScale = (2.0 * gridRadius) / gridDensity;
    
    int64_t sumq0 = 0;
    int64_t sumr0 = 0;
    int64_t sumq2 = 0;
    int64_t sumr2 = 0;

    {   // scope for timer
        PrintTimer test_timer("hex Chambers");
        // fine grained grid centered around zero
        for ( int64_t i = 0; i < gridDensity; ++i ) {
            const double x = i*gridScale - gridRadius;        // -gridRadius..+gridRadius
            
            for ( int64_t j = 0; j < gridDensity; ++j ) {
                const double y = j*gridScale - gridRadius;    // -gridRadius..+gridRadius
                int64_t q, r;
                hex_chambers( x, y, q, r );
                sumq0 += q;
                sumr0 += r;
            }
        }
    }

    {   // scope for timer
        PrintTimer test_timer("hex Cox");
        // fine grained grid centered around zero
        for ( int64_t i = 0; i < gridDensity; ++i ) {
            const double x = i*gridScale - gridRadius;        // -gridRadius..+gridRadius
            
            for ( int64_t j = 0; j < gridDensity; ++j ) {
                const double y = j*gridScale - gridRadius;    // -gridRadius..+gridRadius
                int64_t q, r;
                hex_cox( x, y, q, r );
                sumq2 += q;
                sumr2 += r;
            }
        }
    }
    
    if ( sumq0 != sumq2 || sumr0 != sumr2 )
        printf("profile sums differed, please debug!\n");
}

/******************************************************************************/

// This should take between 6 and 40 seconds, depending on CPU speed
//      ...or if you forgot to turn on the compiler optimization flags ;-)
int main()
{
    printf("Validation Starting\n");
    validate_hex_coords();
    printf("Validation Finished\n");

    printf("Profiling Starting\n");
    profile_hex_conversions();
    printf("Profiling Finished\n");
    
    return 0;
}

/******************************************************************************/
/******************************************************************************/
