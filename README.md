# Hex Coordinates

A little bit of code to test cartesian to hex coordinate conversion, accuracy and performance.

## Instructions
Download the source file, read the source file, compile on the command line (don't forget optimization flags), and run.

## License
Public Domain / Unlicense
