# Hex Coordinates
#

INCLUDE = -I.

CPPFLAGS = -std=c++17 $(INCLUDE) -Ofast

CPPLIBS = -lm

BINARIES = hex_validate

all : $(BINARIES)

hex_validate: hex_validate.cpp
	$(CXX) $(CPPFLAGS) $^ -o $@

test: hex_validate
	./hex_validate

# declare some targets to be fakes without real dependencies
.PHONY : clean

# remove all the stuff we build
clean : 
		rm -f *.o $(BINARIES)
